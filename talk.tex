\documentclass[compress,xcolor=table]{beamer}
%\usepackage[table,x11names]{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{minted}
\usepackage{mathpazo}
\usemintedstyle{xcode}
\usepackage{booktabs}
\usepackage{tcolorbox}
\usepackage[normalem]{ulem}
\graphicspath{{figures/}}
\beamertemplatenavigationsymbolsempty
\setbeamerfont{page number in head/foot}{size=\large}
\setbeamertemplate{footline}[frame number]
\useinnertheme{circles} % bullets are circular
% make paragraphs have vertical space between them, and no indentation
\setlength{\parskip}{\baselineskip}
\setlength{\parindent}{0pt}

% Don't add page count at title page

\let\otp\titlepage
\renewcommand{\titlepage}{\otp\addtocounter{framenumber}{-1}}

% start custom macros
\definecolor{light-gray}{RGB}{194,212,231}
\definecolor{code-bg}{RGB}{245,234,159}
\newcommand{\code}[1]{\colorbox{light-gray}{\texttt{#1}}}
\newtcbox{\badge}{nobeforeafter,colframe=green,colback=green!10!white,box align=base,size=fbox,arc=4pt}
% custom list with no indentation
\newenvironment{noindentlist}{%
\begin{list}{}{%
    \leftmargin=0pt
    \labelwidth=0pt
  }}
{\end{list}}
% custom item with alert+pause
\newcommand\alertitem{\item<alert@+|+->}
% macro for including image that is clipped automagically
\usepackage{adjustbox}
\newcommand{\PrintImage}[3]{%
% #1 : desired width
% #2 : desired height
% #3 : image
    \adjustimage{width={#1},Clip=0 {\height-#2} 0 0}{#3}%
}
% end custom macros



\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\begin{frame}[plain]
\title{GitLab Pipelines for Every Need}
%\subtitle{SUBTITLE}
\author{
	Debsankha Manik
}
    \institute{MPI for Dynamics and Self-Organization, G\"ottingen}
\titlepage
\end{frame}

\begin{frame}{CI/CD Pipelines: Core Idea}
\framesubtitle{Apply well defined operations on the codebase automatically}
\pause{}
On each commit (or according to some fine-tuned criteria)
\pause{}
\begin{enumerate}
    \alertitem A pre-specified  environment is created (typically using \code{docker}).
    \alertitem A pre-defined set of operations are run on the codebase.
    \alertitem Operations may depend on each other, be 
        chronologically ordered, asked to run on parallel $\dots$
\end{enumerate}
\end{frame}

\begin{frame}{Use Case 1: Automated Testing}
\begin{noindentlist}
\alertitem Let's say a codebase has a comprehensive test suite.
\alertitem But new team member cannot run it.
\alertitem Because some tests depend on library \code{foo} being compiled with
\code{bar=True} option. 
\alertitem Nobody knows whether the codebase is deployable or not.
\end{noindentlist}
\begin{noindentlist}
\alertitem \textcolor{blue}{What if we:}
\alertitem 1. Ran our test suite on exactly defined environment on each commit.  
\alertitem 2. Could see which commit caused the test suite to fail.  
\end{noindentlist}
\end{frame}

\begin{frame}{We Can Have Exactly That}
\begin{figure}[!htp]
    \begin{center}
        \PrintImage{\textwidth}{0.8\textheight}{CI-commit-history}
    \end{center}
\end{figure}
\end{frame}

\begin{frame}{Badge screaming loudly if \code{master} is broken}
\begin{figure}[!htp]
    \begin{center}
        \PrintImage{\textwidth}{0.8\textheight}{CI-pipeline-badge}
    \end{center}
\end{figure}

\end{frame}

\begin{frame}{How: Use a CI/CD Pipeline to Run the Tests}
\begin{enumerate}
    \alertitem Choose docker image with base dependencies of your software.
    \alertitem Install additional dependencies using package manager e.g.
        \code{apt}, if needed.
    \alertitem Run the test suite using whatever test runner you want.
        \begin{itemize}
             \alertitem So long the shell gets a $0$ return code on sucess and 
                 nonzero on failure.  
        \end{itemize}
\end{enumerate}
\end{frame}

\begin{frame}[fragile]{How to Set This Up?}
    \inputminted[bgcolor=code-bg]{yaml}{snippets/ci-testing.yml}
\end{frame}

\begin{frame}{Use Case 2: Integration Tests}
We had some tests that ``simulates'' our deployed system, i.e.
\begin{enumerate}
\item Installs all the componenets of our software.
\item Spins up the server.
\item Creates a few clients.
\item The clients fires a few hundred requests.
\item We check if the server was able to handle all these requests, satisfying 
    some predefined criteria. 
\end{enumerate}

\pause{}
Running these tests didn't fit in the basic CI/CD paradigm:
\begin{itemize}
\item Takes too long to run.
\item We didn't need this to run on every commit to every branch.
\end{itemize}

\pause{}
We wanted a nighly run of these ``integration tests'' on master.
\end{frame}

\begin{frame}{We Ended Up With This}
\begin{figure}[!htp]
    \begin{center}
        \PrintImage{\textwidth}{0.8\textheight}{integration_testing_badge}
    \end{center}
\end{figure}


\end{frame}

\begin{frame}[fragile]{How to Set This Up?}
\begin{noindentlist}
    \alertitem Specify an environment as before using \code{docker}.
    \alertitem Specify a new \code{stage} in the CI pipeline that is to be 
        triggered only on schedule, not automatically on each commit.
    \item<+->
\begin{minted}[bgcolor=code-bg]{yaml}
only:
  - schedules
  - master
\end{minted}
    \alertitem Generate custom badges displaying this stage passes/fails 
        using \code{artifact}s (more on this later).

    \item<+->
\begin{minted}[bgcolor=code-bg]{yaml}
artifacts:
  paths:
    - public # this is where we save
             # custom made badges
\end{minted}
\end{noindentlist}
\end{frame}


\begin{frame}{Use Case 3: \sout{Writing a Paper} Making This Presentation}
\framesubtitle{https://gitlab.gwdg.de/dmanik/gitlab-ci-talk}
\begin{figure}[!htp]
    \begin{center}
        \PrintImage{\textwidth}{0.9\textheight}{ci-pdf}
    \end{center}
\end{figure}


\end{frame}
\begin{frame}[fragile]{How to Set This Up?}
\begin{noindentlist}
    \alertitem Use a docker image with \code{texlive} (here \code{blang/latex})
    \alertitem Define a job that invokes \code{latexmk} to compile the PDF.
    \item<+->
\begin{minted}[bgcolor=code-bg]{yaml}
makepdf:
  stage: build
  script:
    - latexmk -e '$pdflatex=q/pdflatex %O\
      -shell-escape %S/' -pdf
\end{minted}
    \alertitem Use \code{job artifacts} to store the compiled PDF.
\item<+->
\begin{minted}[bgcolor=code-bg]{yaml}
artifacts:
  paths:
    - "*.pdf"
\end{minted}
\alertitem We ask GitLab to store all files matching the wildcard \code{*.pdf}.
\alertitem The PDF is then accessible under the URL
    \code{\footnotesize\url{<repo_root>/-/jobs/artifacts/master/raw/talk.pdf?job=makepdf}}.
\end{noindentlist}

\end{frame}


\begin{frame}{Outlook}
\pause{}
\begin{noindentlist}
\alertitem Multi project pipelines (build/test the full stack divided across repositories).
\alertitem Automatically run test suite on  merge requests.
\begin{itemize}
    \item No need to waste time reviewing a MR if the test suite do not pass.
\end{itemize}
    \alertitem Need to download large amounts of data for tests to run: use \code{cache}, 
    maybe docker \code{volume}s?
    \alertitem Skipping the pipeline on certain commits (\code{except} keyword 
    in CI config). 
    \begin{itemize}
        \item e.g. changing the \code{README}.
    \end{itemize}
\alertitem Publish your package to PyPi, deploy to staging/production.  
\alertitem Reproducible papers?
\end{noindentlist}
\end{frame}


\end{document}
