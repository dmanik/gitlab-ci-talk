* Find texlive docker image.
* Add .gitlab-ci.yml
* talk about badges.
* Talk about test running policy: each commit/only master/only on tags...
* Talk about artifacts.
* talk about paper repo.
* talk about jupytext
* talk about badges/anybadge/dependent stages.
* talk about multi-project pipelines. (monorepo vs multi repo).
* talk aboout `skipci`  in commit messages.
* moonshot stuff: use a runner to submit jobs to a cluster..
* CI/CD for external repo?
* merge requests

Setting up your own runner
--------------------------
1. 

Steps
-----
* Settings -> Pipelines -> enable
* Enable a runner
* Yes, it *is* that simple.

Story
-----
1. Audience poll: how many of you use CI/CD pipelines?
2. [x] Core idea behind CICDP: predefined operations automatically applied on the codebase.
  - Exactly defined environment (docker image).
  - Exactly defined set of tasks (some parallel, some serial).
3. [x] Scenario one: automated testing
  - Let's say our codebase has a comprehensive test suite, but:
  - Passes on My Machine^TM.
  - Oh I forgot to tell you: tests for module `foo` fail unless `libbar` is compiled with `-DSPAM=true` flag.
4. [x] CICDP for automated testing:
  - Specify a docker image (e.g. python 3.6 and python 2.7)
    + TODO: check if multiple test environments are possible.
  - Setup the test environment
    + Install system packages using package manager, if necessary.
  - Run your test suite.
    + Web interface will show for which commits the test suite failed.
    + Nice badge.
5. [x] View on website.
6. [x] CICDP config pseudocode.
7. Some optimizations
  - Pipeline takes too long because:
    + have to download files: use `cache`
    + have to compile stuff: use `cache`
  - Secrets management via env vars.
  - Don't want to run pipeline for this commit (added docstring etc.)
    + TODO: find out if `skipci` exists.
  - Automatically run test suite on merge requests (TODO: figure out how).
8. Great thing: CICDP configuration is a plaintext file, is under version control.
9. Scenario two: integration tests
  - Test all the components of a stack.
  - Each components have unittests (and CI enabled).
  - But we want to know if all the components work together.
  - We wrote some tests that "simulates" a production environment. 
  - But the tests take too long to run. 
  - We also don't want to run these tests to run on each commit to each branch.
  - We want to run them only on `master` each night.
10. CICDP configuration
  - branch/tag specific execution.
  - scheduled execution.
  - special badge generation.
11. CICDP config pseudocode.
12. View on website.
13. Scenario three: write a paper.
  - Use docker image with a full TeX installation.
  - Define build stage with `latexmk`.
  - (Optional): Define plot generation stage.
  - Store the PDF using artifacts.
14. Even more cool stuff one could do (should one?):
  - Package ones software and upload to (open source) repository.
  - 

